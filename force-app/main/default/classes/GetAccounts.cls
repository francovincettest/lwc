public with sharing class GetAccounts {
    @AuraEnabled(cacheable=true)
    public static List<Account> getAccounts( String searchKey, String sortBy, String sortDirection ) {
        system.debug('!!!' + searchKey + ' ' + sortBy + ' ' + sortDirection);
        String query = 'SELECT Id, Name, Phone, Website FROM Account ';
        if ( searchKey != null && searchKey != '' ) {
            String key = '%' + searchKey + '%';
            query += ' WHERE Name LIKE :key';  // Modified again
        }
  
        if ( sortBy != null && sortDirection != null ) {
            query += ' ORDER BY ' + sortBy + ' ' + sortDirection;
        }
        return Database.query( query );
    }
 }
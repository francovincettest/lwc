@isTest
private class GetAccountsTest {

    static testMethod void runPositiveTestCases() {
        
        System.debug('Inserting 1  Account... (single record validation)');
        
        Account testAccount = new Account(Name = 'testAccount');
        insert testAccount;
        Test.startTest();
        list<Account> lstAccount = GetAccounts.GetAccounts('testAccount',null,null);
        Test.stopTest();   
        System.assertEquals(lstAccount.size(), 1);
    
    } 
    
} 